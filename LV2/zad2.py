import numpy as np
import matplotlib.pyplot as plt

#a)
data=np.loadtxt('data.csv', skiprows=1, delimiter=',')
print("Mjerenja su izvršena na ",len(data),"osoba")

#b)
plt.scatter(data[:,1],data[:,2],s=2)
plt.title("zadatak b")
plt.show()

#c)
plt.scatter(data[::50,1], data[::50,2],s=5)
plt.title("zadatak c")
plt.show()
          
#d)
print("Minimalna vrijdnost visine:",data[:,1].min() )
print("Maksimalna vrijednost visine :",data[:,1].max())
print("Srednja vrijednost visine:",data[:,1].mean())

#e) 
m=(data[:,0] == 1)
z=(data[:,0] == 0)

print("Miniminalna vrijednost visine žena",np.min(data[z,1]))
print("Maksimalna vrijednost visine žena",np.max(data[z,1]))
print("Srednja vrijednost visine žena",np.mean(data[z,1]))

print("Miniminalna vrijednost visina muškaraca",np.min(data[m,1]))
print("Maksimalna vrijednost visine muškaraca",np.max(data[m,1]))
print("Srednja vrijednost visine muškaraca",np.mean(data[m,1]))
