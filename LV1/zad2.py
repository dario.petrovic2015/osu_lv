try:
    ocjena=float(input('Ocjena: '))
    try:
        if (ocjena<0.0 or ocjena>1.0):
            raise ValueError
        if(ocjena<=1.0 and ocjena>=0.9):
            print("A")
        elif(ocjena<0.9 and ocjena>=0.8):
            print("B")
        elif(ocjena<0.8 and ocjena>=0.7):
            print("C")
        elif(ocjena<0.7 and ocjena>=0.6):
            print("D")
        elif(ocjena<0.6 and ocjena>=0.0):
            print("F")
    except ValueError:
        print("Broj nije u intervalu.")
except ValueError:
    print("Unos nije broj.")