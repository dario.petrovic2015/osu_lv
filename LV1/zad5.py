ham=[]
spam=[]
suma_ham=0
suma_spam=0
prosjek_ham=0.0
projek_spam=0.0
usklicnik=0

fhand = open("LV1\SMSSpamCollection.txt")
for line in fhand:
    oznaka, sadrzaj=line.strip().split("\t")
    if oznaka == "ham":
      ham.append(sadrzaj)
    elif oznaka == "spam":
       spam.append(sadrzaj)

for poruka in ham:
   suma_ham+=len(poruka.split())
prosjek_ham=suma_ham/len(ham)
print("Prosjecan broj rijeci u ham porukama je:", prosjek_ham)

for poruka in spam:
   suma_spam+=len(poruka.split())
prosjek_spam=suma_spam/len(spam)
print("Prosjecan broj rijeci u spam porukama je:", prosjek_spam)

for poruka in spam:
   if poruka.endswith("!"):
      usklicnik+=1
print("Broj spam poruka koje zavrsavaju usklicnikom:", usklicnik)

fhand.close()